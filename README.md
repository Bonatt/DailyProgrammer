### Daily Programmer Challenges via Reddit

As seen at [r/dailyprogrammer](https://www.reddit.com/r/dailyprogrammer/).

"Dailyprogrammer is about challenging programmers of all skill level with weekly programming challenges. 3 challenges a week are posted at increasing difficulty. Solutions are peer reviewed and redditors can ask for the community for feedback and comments."

I don't really keep up with posted challenges (because I just discovered it Janurary 2018 and because I have other priorities),
but when I do feel piqued I (will) try them out. Those solutions are put here.
