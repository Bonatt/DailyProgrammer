### [[2018-01-30] Challenge #349 [Intermediate] Packing Stacks of Boxes](https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/)
>>>
__Description__

You run a moving truck business, and you can pack the most in your truck when you have stacks of equal size - no slack space. So, you're an enterprising person, and you want to write some code to help you along.

__Input Description__

You'll be given two numbers per line. The first number is the number of stacks of boxes to yield. The second is a list of boxes, one integer per size, to pack.

Example:

```
3 34312332
```
That says "make three stacks of boxes with sizes 3, 4, 3, 1 etc".

__Output Description__

Your program should emit the stack of boxes as a series of integers, one stack per line. From the above example:

```
331
322
34
```
If you can't make equal sized stacks, your program should emit nothing.


__Challenge Input__

```
3 912743471352
3 42137586
9 2
4 064876318535318
```

__Challenge Output__

```
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
```
>>>

### Now my attempt...

I employed a [backtracking](https://en.wikipedia.org/wiki/Backtracking)
([depth first search](https://en.wikipedia.org/wiki/Depth-first_search)).

Start filling first stack.
When first stack is too large, move offending box to second stack.
Add next box to first stack. If this offends, move to second stack.
When second stack is too large, move offending box to third stack...
When last stack is too large, remove (pop) all boxes from last stack,
 and move last box added to last-1 stack to last stack.
Readd first box from last stack (before pop) to first stack.
Repeat... until all stacks are not too large (and since there was a check
 prior to packing, all N stacks will be same size).
See my code for more specific doumentation. I hope it makes sense; returning to
this code, it is kind of opaque.

Here's my output. I don't know if the challenge _required_ them to be as
square as possible. Mine are (coindicentally(?)) but solutions from other
individuals were not.

```
9124
7342
7135

426
138
75

(nothing)

0647
863
1853
5318
```


Tracked output for first input:
```
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0]  Stacks = [[9], [], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0]  Stacks = [[9, 1], [], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0]  Stacks = [[9, 1, 2], [], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 0]  Stacks = [[9, 1, 2, 7], [], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1]  Stacks = [[9, 1, 2], [7], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0]  Stacks = [[9, 1, 2, 4], [7], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 0]  Stacks = [[9, 1, 2, 4, 3], [7], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1]  Stacks = [[9, 1, 2, 4], [7, 3], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 0]  Stacks = [[9, 1, 2, 4, 4], [7, 3], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 0]  Stacks = [[9, 1, 2, 4, 7], [7, 3, 4], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 7], []]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4], [7]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 0]  Stacks = [[9, 1, 2, 4, 1], [7, 3, 4], [7]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1], [7]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 0]  Stacks = [[9, 1, 2, 4, 3], [7, 3, 4, 1], [7]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1, 3], [7]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1], [7, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 0]  Stacks = [[9, 1, 2, 4, 5], [7, 3, 4, 1], [7, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1, 5], [7, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1], [7, 3, 5]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 2, 0]  Stacks = [[9, 1, 2, 4, 2], [7, 3, 4, 1], [7, 3, 5]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1, 2], [7, 3, 5]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 1, 2, 2, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 1], [7, 3, 5, 2]]
Backtracking...
Backtracking...
Backtracking...
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4], [7, 1]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 0]  Stacks = [[9, 1, 2, 4, 3], [7, 3, 4], [7, 1]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 3], [7, 1]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4], [7, 1, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2, 0]  Stacks = [[9, 1, 2, 4, 5], [7, 3, 4], [7, 1, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 5], [7, 1, 3]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2, 2]  Stacks = [[9, 1, 2, 4], [7, 3, 4], [7, 1, 3, 5]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2, 2, 0]  Stacks = [[9, 1, 2, 4, 2], [7, 3, 4], [7, 1, 3, 5]]
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]  Tree = [0, 0, 0, 1, 0, 1, 1, 2, 2, 2, 2, 1]  Stacks = [[9, 1, 2, 4], [7, 3, 4, 2], [7, 1, 3, 5]]
```

