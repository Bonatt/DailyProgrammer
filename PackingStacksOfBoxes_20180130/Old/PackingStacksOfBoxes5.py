'''
[2018-01-30] Challenge #349 [Intermediate] Packing Stacks of Boxes
https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/

Description
You run a moving truck business, and you can pack the most in your truck when you have stacks of equal size - no slack space. So, you're an enterprising person, and you want to write some code to help you along.

Input Description
You'll be given two numbers per line. The first number is the number of stacks of boxes to yield. The second is a list of boxes, one integer per size, to pack.

Example:

3 34312332
That says "make three stacks of boxes with sizes 3, 4, 3, 1 etc".

Output Description
Your program should emit the stack of boxes as a series of integers, one stack per line. From the above example:

331
322
34
If you can't make equal sized stacks, your program should emit nothing.

Challenge Input
3 912743471352
3 42137586
9 2 
4 064876318535318

Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''


























'''
#line = '3 912743471352\n'
#line = '3 42137586\n'
#line = '9 2\n'
line = '4 064876318535318\n'
line = '3 34312332\n' # Example
#line = '2 1111111119\n' # Another edge case
n, boxes = line.strip().split()
N, Boxes = int(n), sorted([int(i) for i in boxes], reverse=True)

# If the boxes cannot evenly bin into stacks without some remainder, 
# or if there are more stacks than boxes, skip.
if (sum(Boxes)%N > 0) or (N > len(Boxes)):
    print('(nothing)')

# All stacks must sum to this stack size.
StackSize = sum(Boxes)//N
'''


### Backtracking (Depth First Search)
# From https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/dtjb1fz/
# Start filling first stack.
# When first stack is too large, move offending box to second stack.
# Add next box to first stack. If this offends, move to second stack.
# When second stack is too large, move offending box to third stack...
# When last stack is too large, remove (pop) all boxes from last stack,
#  and move last box added to last-1 stack to last stack.
# Readd first box from last stack (before pop) to first stack. 
# Repeat... until all stacks are not too large (and since there was a check
#  prior to packing, all N stacks will be same size).
def Pack(N, Boxes):
    # Stack index/key/tree for each box, e.g. for Boxes=[9,...] and s=[0,...], 
    #  Box 9 to be packed into stack 0. 
    #  Set as [0] to start at stack 0.
    Tree = [0]
    # List of successfully packed Boxes into N stacks. This is returned.
    Packed = []

    # Only breaks when valid solution is found.
    while True:
        # (Re)Create list of N stacks. Wipes previous Tree and restarts
        #  ~one branch back.
        #  Repopulate this list with currently-valid Tree
        #  (created during of previous loop)
        Stacks = [[] for i in range(N)]
        for box, stack in zip(Boxes, Tree):  
            Stacks[stack].append(box)
        print('Boxes =', Boxes, ' Tree =',Tree, ' Stacks =',Stacks)

        # If current/tail stack is too large after new box was added,  
        #  remove box(es) until back to previous stack. 
        #  Move th(os)e box(es) to next stack.
        #  While final stack is too large, remove boxes.
        if sum(Stacks[Tree[-1]]) > sum(Boxes)//N:
            while Tree[-1] == N-1:
                #print(Tree[-1], N-1)
                print('Backtracking...')
                Tree.pop()
                #print('Boxes =', Boxes, ' Tree =',Tree, ' Stacks =',Stacks)
            Tree[-1] += 1
            #print('Boxes =', Boxes, ' Tree =',Tree, ' Stacks =',Stacks)
        # If solution found, i.e. all boxes gone through without the above 
        #  happening, append stack strings to be returned, and break.
        elif len(Tree) == len(Boxes):
            for stack in Stacks:
                Packed.append(stack) #''.join(str(i) for i in stack))
            break

        # Current solution is valid (no stacks too large), but there are more
        #  boxes to stack and current stack arrangement may be invalidated.
        # Append Tree to start at 0 for new box to be stacked 
        #  (at beginning of next loop, i.e. right now).
        else:
            Tree.append(0)
    
    return Packed



### Take file of form '3 912743471352\n', etc., preprocess it and pack.
def PackingStacksOfBoxes(filename):#, delimiter=' '):

    with open(filename) as f:
        for line in f:

            #print(line)

            # n = '3', boxes = '912743471352'
            #  strip() strips white spaces and newlines
            #  split() splits string into strings separated by delimiter
            n, boxes = line.strip().split()#delimiter)

            # N = 3, Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]
            N, Boxes = int(n), [int(i) for i in boxes]

            # If Boxes cannot sum into N stacks without remainder, 
            #  or if there are more stacks than boxes,
            #  there exists no solution. Skip (continnue).
            if (sum(Boxes)%N > 0) or (N > len(Boxes)):
                print('(nothing)')
                print()
                continue

            # All Boxes can pack neatly, and now must find how. Print results.
            Packed = Pack(N,Boxes)
            for stack in Packed:
                print(''.join(str(i) for i in stack))
            print()


# '3 912743471352', e.g.
filename = 'ChallengeInput'

print()
PackingStacksOfBoxes(filename)


'''
Pack(3, sorted([int(i) for i in '42137586']))
'''
