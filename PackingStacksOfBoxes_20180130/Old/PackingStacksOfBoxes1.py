'''
[2018-01-30] Challenge #349 [Intermediate] Packing Stacks of Boxes
https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/

Description
You run a moving truck business, and you can pack the most in your truck when you have stacks of equal size - no slack space. So, you're an enterprising person, and you want to write some code to help you along.

Input Description
You'll be given two numbers per line. The first number is the number of stacks of boxes to yield. The second is a list of boxes, one integer per size, to pack.

Example:

3 34312332
That says "make three stacks of boxes with sizes 3, 4, 3, 1 etc".

Output Description
Your program should emit the stack of boxes as a series of integers, one stack per line. From the above example:

331
322
34
If you can't make equal sized stacks, your program should emit nothing.

Challenge Input
3 912743471352
3 42137586
9 2 
4 064876318535318

Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''



'''
Input = [   [3, '912743471352'],
            [3, '42137586'],
            [9, '2'],
            [4, '064876318535318'] ]

box = pd.DataFrame(Input, columns=['n','list'])
list(Input[0][1])
[[int(i) for i in Input[j][1]] for j in range(len(Input))]
'''

import pandas as pd

# Box df with 'n boxes' and 'list of box sizes'
box = pd.read_csv('ChallengeInput', delimiter=' ', names=('n','list'), dtype={'n':'int64', 'list':'str'})

'''
n = box.n.values
list = box.list.values

# '110' --> 3
ListLen = [len(i) for i in list]
# ['110'] --> [1,1,0]
List = [[int(i) for i in j] for j in list]
# [1,1,0] --> 2
ListSums = [sum(i) for i in List]
# 2 --> 2/n = 2/3 = 0.66 
# n sets of numbers must sum to these numbers without remaider.
ListAvg = ListSums/n #[sum(i)/j for i,j in zip(List,n)]
# How many boxes per stack.
BoxesPerStack = ListLen/n
'''






nStacks = box.n.values
Boxes = [[int(i) for i in j] for j in box.list.values]
nBoxes = [len(i) for i in box.list.values]
BoxesPerStack = nBoxes/nStacks
StackSums = [sum(i) for i in Boxes]

# Quotient rounded, remainder
StackSizes = [(nb//ns, nb%ns) for nb,ns in zip(nBoxes,nStacks)] #divmod(nBoxes,nStacks)


df = pd.read_csv('ChallengeInput', delimiter=' ', names=('nStacks', 'ListOfBoxes'))
df['Boxes'] = [[int(i) for i in str(j)] for j in df['ListOfBoxes'].values]
df['nBoxes'] = [len(i) for i in df['Boxes'].values]
df['BoxSums'] = [sum(i) for i in df['Boxes']]
df['StackSums'] = (df['BoxSums']/df['nStacks']).astype('int')
#df['StackSizes'] = [[nb//sb, nb%sb] for nb,sb in zip(df['nBoxes'],df['nStacks'])]
# From https://stackoverflow.com/questions/4119070/how-to-divide-a-list-into-n-equal-parts-python
# Splits an array into specific len list with remainder as final list
lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]

a = [lol(i,j) for i,j in zip(df['Boxes'],df['nStacks'])]
b = [[len(i) for i in j] for j in a]

df['StackSizes'] = b









'''
# From https://stackoverflow.com/questions/2130016/splitting-a-list-into-n-parts-of-approximately-equal-length
def chunkify(lst,n):
    return [lst[i::n] for i in range(n)]
i=3; chunkify(df['Boxes'][i],df['StacksOfBoxes'][i])

# Literally the above by by numpy: puts arrays into equal bins, but doesn't make equal sum.
a = [np.array_split(i,j) for i,j in zip(df['Boxes'], df['StacksOfBoxes'])]
b = [[len(i) for i in j] for j in a]
'''





filename = 'ChallengeInput'


def PackingStacksOfBoxes(filename):

    with open(filename) as f:
        for line in f:
            print(line)




PackingStacksOfBoxes(filename)




'''
Challenge Input
3 912743471352
3 42137586
9 2
4 064876318535318


Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''
