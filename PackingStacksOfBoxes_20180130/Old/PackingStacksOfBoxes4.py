'''
[2018-01-30] Challenge #349 [Intermediate] Packing Stacks of Boxes
https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/

Description
You run a moving truck business, and you can pack the most in your truck when you have stacks of equal size - no slack space. So, you're an enterprising person, and you want to write some code to help you along.

Input Description
You'll be given two numbers per line. The first number is the number of stacks of boxes to yield. The second is a list of boxes, one integer per size, to pack.

Example:

3 34312332
That says "make three stacks of boxes with sizes 3, 4, 3, 1 etc".

Output Description
Your program should emit the stack of boxes as a series of integers, one stack per line. From the above example:

331
322
34
If you can't make equal sized stacks, your program should emit nothing.

Challenge Input
3 912743471352
3 42137586
9 2 
4 064876318535318

Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''















### From https://stackoverflow.com/questions/7392143/python-implementations-of-packing-algorithm
'''
# Container for items that keeps a running sum
class Stack(object):
    def __init__(self):
        self.items = []
        self.sum = 0

    def append(self, item):
        self.items.append(item)
        self.sum += item
    
    # Printable representation
    def __str__(self):
        return 'Stack(sum=%d, items=%s)' % (self.sum, str(self.items))
        #return ''.join(str(i) for i in self.items)


# The packing algorithm
def Pack(values, maxValue):
    values = sorted(values, reverse=True)
    stacks = []

    for item in values:
        # Try to fit item into a stack
        for stack in stacks:
            if stack.sum + item <= maxValue:
                #print 'Adding', item, 'to', stack
                stack.append(item)
                break
        else:
            # item didn't fit into any stack, start a new stack
            #print 'Making new stack for', item
            stack = Stack()
            stack.append(item)
            stacks.append(stack)

    return stacks



#line = '3 912743471352\n'
#line = '3 42137586\n'
#line = '9 2\n'
line = '4 064876318535318\n'
#line = '2 1111111119\n'
n, boxes = line.strip().split()
N, Boxes = int(n), [int(i) for i in boxes]
StackSum = sum(Boxes)//N

stacks = Pack(Boxes,StackSum)
for s in stacks: print(s)
'''














'''
def PackingStacksOfBoxes(filename):#, delimiter=' '):

    with open(filename) as f:
        for line in f:
            
            # n = '3', boxes = '912743471352'
            # strip() strips white spaces and newlines
            # split() splits one string into two strings separated by delimiter
            n, boxes = line.strip().split()#delimiter)

            # N = 3, Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]
            N, Boxes = int(n), [int(i) for i in boxes]            

            # If the boxes cannot bin into stacks without remainder, 
            # or if there are more stacks than boxes, skip.
            if (sum(Boxes)%N > 0) or (N > len(Boxes)):
                print('(nothing)')
                continue


            # Each stack (N stacks) must sum to StackSum: 
            # 9+1+2+4 = 7+3+4+2 = 7+1+3+5 = 16 = Stacksum
            StackSize = sum(Boxes)//N #int(sum(Boxes)/N)

            
            Pack(N,Boxes)
    
          
# '3 912743471352', e.g.
filename = 'ChallengeInput'              

PackingStacksOfBoxes(filename)
'''




'''
#line = '3 912743471352\n'
#line = '3 42137586\n'
#line = '9 2\n'
line = '4 064876318535318\n'
#line = '2 1111111119\n'
n, boxes = line.strip().split()
N, Boxes = int(n), sorted([int(i) for i in boxes], reverse=True)

# If the boxes cannot evenly bin into stacks without some remainder, or if there are more stacks than boxes, skip.
if (sum(Boxes)%N > 0) or (N > len(Boxes)):
    print('(nothing)')

# All stacks must sum to this stack size.
StackSize = sum(Boxes)//N
'''


# Backtracking Packing (Depth First Search)
def Pack(N, Boxes):
    # Stack index for each box, e.g. for Boxes=[9,...] and s=[0,...], 
    # Box 9 to be packed into stack 0. 
    # Set as [0] to start at stack 0.
    stackkey = [0]
    # List of successfully packed Boxes into N stacks. This is returned.
    Packed = []

    while True:
        # (Re)Create list of N stacks. 
        stacks = [[] for i in range(N)]
        for box, stack in zip(Boxes, stackkey):  
            #print(box,stack) 
            stacks[stack].append(box)
        print('stackkey =',stackkey,'stacks =',stacks)

        # If current/terminal stack is too large, 
        # remove box(es) until stack is not too large. 
        # Move th(os)e box(es) to new/next stack.
        if sum(stacks[stackkey[-1]]) > sum(Boxes)//N:
            while stackkey[-1] == N-1:
                print('Backtracking...')
                stackkey.pop()
            stackkey[-1] += 1

        # If solution found, i.e. all boxes gone through without the above 
        # happening, append stack strings to be returned, and break.
        elif len(stackkey) == len(Boxes):
            for stack in stacks:
                Packed.append(''.join(str(i) for i in stack))
            break

        # Current solution is valid (no stacks too large), but there are 
        # more boxes to stack and previous stacks may be invalidated.
        else:
            stackkey.append(0)
        #print('stackkey =',stackkey,'  stacks =',stacks)
    
    return Packed






def PackingStacksOfBoxes(filename):#, delimiter=' '):

    with open(filename) as f:
        for line in f:

            #print(line)

            # n = '3', boxes = '912743471352'
            # strip() strips white spaces and newlines
            # split() splits string into strings separated by delimiter
            n, boxes = line.strip().split()#delimiter)

            # N = 3, Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]
            N, Boxes = int(n), [int(i) for i in boxes]

            # If the boxes cannot bin into stacks without remainder, 
            # or if there are more stacks than boxes, skip
            if (sum(Boxes)%N > 0) or (N > len(Boxes)):
                print('(nothing)')
                print()
                continue

            # Each stack (N stacks) must sum to StackSum: 
            # 9+1+2+4 = 7+3+4+2 = 7+1+3+5 = 16 = Stacksum
            StackSize = sum(Boxes)//N #int(sum(Boxes)/N)


            Packed = Pack(N,Boxes)
            for i in Packed:
                print(i)
            print()


# '3 912743471352', e.g.
filename = 'ChallengeInput'

print()
PackingStacksOfBoxes(filename)














# To find out sizes of required stacks...
#Boxes.sort(reverse=True)
#StackedBoxes_unordered = [Boxes[i:i+len(Boxes)//N] for i in range(0,len(Boxes),len(Boxes)//N)]

# First check if any box fills a stack by itself.
'''
Stacks = []
stack = ''
for i,box in enumerate(Boxes):
    if box == StackSum:
        print(box)
        stack += str(box)
        #stack.append( [box] )
'''


#from itertools import permutations, combinations


### Either try permutation, backtracking, or some form of bin packing, or breadth/depth first search, or...?



'''
n, boxes = line.strip().split() #input().split()

n = int(n)
boxes = sorted(map(int, boxes), reverse=True) # sorting and reversing speeds up backtracking
size = sum(boxes) // n

s = [0] # s lists the stack for each box
while True:
    d = [[] for _ in range(n)] # d lists the boxes for each stack
    for box, stack in zip(boxes, s):
        d[stack].append(box)
    if sum(d[s[-1]]) > size: # one of the stacks is too large
        while s[-1] == n-1:
            s.pop()
        if s[-1] == len(s) - 1: # if [0, 1, ...] fails, so will [0, 2, ...]
            print('(nothing)')
            break
        s[-1]+=1
    elif len(s) == len(boxes): # solution found
        for stack in d:
            print(''.join(map(str, stack)))
        break
    else: # solution is correct but partial, dig deeper
        s.append(0)

'''
