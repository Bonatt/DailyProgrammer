'''
[2018-01-30] Challenge #349 [Intermediate] Packing Stacks of Boxes
https://www.reddit.com/r/dailyprogrammer/comments/7ubc70/20180130_challenge_349_intermediate_packing/

Description
You run a moving truck business, and you can pack the most in your truck when you have stacks of equal size - no slack space. So, you're an enterprising person, and you want to write some code to help you along.

Input Description
You'll be given two numbers per line. The first number is the number of stacks of boxes to yield. The second is a list of boxes, one integer per size, to pack.

Example:

3 34312332
That says "make three stacks of boxes with sizes 3, 4, 3, 1 etc".

Output Description
Your program should emit the stack of boxes as a series of integers, one stack per line. From the above example:

331
322
34
If you can't make equal sized stacks, your program should emit nothing.

Challenge Input
3 912743471352
3 42137586
9 2 
4 064876318535318

Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''



#'''
import pandas as pd

df = pd.read_csv('ChallengeInput', delimiter=' ', names=('nStacks', 'ListOfBoxes'))
df['Boxes'] = [[int(i) for i in str(j)] for j in df['ListOfBoxes'].values]
df['nBoxes'] = [len(i) for i in df['Boxes'].values]
df['BoxSums'] = [sum(i) for i in df['Boxes']]
df['StackSums'] = (df['BoxSums']/df['nStacks']).astype('int')
#df['StackSizes'] = [[nb//sb, nb%sb] for nb,sb in zip(df['nBoxes'],df['nStacks'])]
# From https://stackoverflow.com/questions/4119070/how-to-divide-a-list-into-n-equal-parts-python
# Splits an array into specific len list with remainder as final list
lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]

a = [lol(i,j) for i,j in zip(df['Boxes'],df['nStacks'])]
b = [[len(i) for i in j] for j in a]

df['StackSizes'] = b
#'''







# '3 912743471352', e.g.
filename = 'ChallengeInput'

def PackingStacksOfBoxes(filename):#, delimiter=' '):

    with open(filename) as f:
        for line in f:
            
            # n = '3', boxes = '912743471352'
            # strip() strips white spaces and newlines
            # split() splits one string into two strings separated by delimiter
            n, boxes = line.strip().split()#delimiter)
            #print(n,boxes)

            # N = 3, Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]
            N = int(n)
            Boxes = [int(i) for i in boxes]            

            # Each stack (N stacks) must sum to StackSum: 9+1+2+4 = 7+3+4+2 = 7+1+3+5 = 16 = Stacksum
            StackSum = sum(Boxes)//N #int(sum(Boxes)/N)

            # If the number of stacks (N) is greater than the number of boxes, skip.
            if StackSum == 0:
                print('(nothing)')
                continue

            # Sorted Boxes in place. Not in place is sorted(...)
            #Boxes.sort()

            StackedBoxes_unordered = [Boxes[i:i+len(Boxes)//N] for i in range(0,len(Boxes),len(Boxes)//N)]
            #print(type(N), type(Boxes), type(Boxes[0]))

            StackedBoxes = StackedBoxes_unordered[:]
          
              

PackingStacksOfBoxes(filename)

# maxlen = len(Boxes)//N + len(Boxes)%N

boxes = '912743471352'



StackedBoxes_unordered = [Boxes[i:i+len(Boxes)//N] for i in range(0,len(Boxes),len(Boxes)//N)]
from copy import copy, deepcopy
StackedBoxes = deepcopy(StackedBoxes_unordered)

'''
for stack in StackedBoxes:
    for box in stack:
        print(box)
'''
Boxes_sorted = sorted(Boxes)
for row,stack in enumerate(StackedBoxes):
    for column,box in enumerate(stack):
        StackedBoxes[row][column] = Boxes_sorted.pop(-1)
        



N = 3
Boxes = [9, 1, 2, 7, 4, 3, 4, 7, 1, 3, 5, 2]
Boxes.sort()
StackSum = int(sum(Boxes)/N); StackSum
x = len(Boxes)//4; x #3
Boxes
for i in range(x): Boxes[i*2],Boxes[-i*2-1] = Boxes[-i*2-1], Boxes[i*2]; Boxes

StackedBoxes_unordered = [Boxes[i:i+len(Boxes)//N] for i in range(0,len(Boxes),len(Boxes)//N)]


'''
Challenge Input
3 912743471352
3 42137586
9 2
4 064876318535318


Challenge Output
9124
7342
7135

426
138
75

(nothing)

0665
4733
8315
881
'''
